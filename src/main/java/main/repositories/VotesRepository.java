package main.repositories;

import main.model.PostVotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VotesRepository extends CrudRepository<PostVotes, Integer> {
    PostVotes findByUserIdAndPostId(@Param("user_id") Integer userId, @Param("post_id") Integer postId);

    List<PostVotes> findByPostId(@Param("post_id") Integer postId);
}
