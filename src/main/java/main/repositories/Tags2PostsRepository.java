package main.repositories;

import main.model.Posts;
import main.model.Tag2Post;
import main.model.Tags;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface Tags2PostsRepository extends PagingAndSortingRepository<Tag2Post, Integer> {
    List<Posts> findByTagId(@Param("tag_id") int tagId, Pageable pageable);

    List<Posts> findByTagId(@Param("tag_id") int tagId, Sort sort);

    List<Tag2Post> findByTagId(@Param("tag_id") int tagId);

    List<Tags> findByPostId(@Param("post_id") int postId, Pageable pageable);
}
