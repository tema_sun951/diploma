package main.repositories;

import main.model.Tags;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TagsRepository extends CrudRepository<Tags, Integer> {

    List<Tags> findByNameContaining(String name, Sort sort);
    Tags findByName(String name, Sort sort);

    Tags findByName(String name);
}
