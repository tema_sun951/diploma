package main.repositories;

import main.model.Posts;
import main.model.Tags;
import main.model.constants.Constants;
import main.model.types.PostStatus;
import main.model.types.PostsTypes;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface PostsRepository extends PagingAndSortingRepository<Posts, Integer> {
    List<Posts> findAll(Sort sort);

    List<Posts> findAllByModerationStatus(PostsTypes moderationStatus, Pageable pageable);

    List<Posts> findByTitleContainingOrTextContaining(String title, String text, Pageable pageable);

    List<Posts> findByTimeBetween(Date timeStart, Date timeEnd, Pageable pageable);

    List<Posts> findByTime(Date time);

    List<Posts> findByModerationStatusAndIsActive(@Param("moderation_status") PostsTypes moderationStatus, @Param("is_active") Integer isActive, Pageable pageable);

    List<Posts> findByModerationStatusAndIsActive(@Param("moderation_status") PostsTypes moderationStatus, @Param("is_active") Integer isActive);

    List<Posts> findByUserIdAndPostStatus(@Param("user_id") int userId, @Param("post_status") PostStatus postStatus, Pageable pageable);

    List<Posts> findByTags(@Param("tags") Tags tag, Pageable pageable);

    List<Posts> findByIsActive(@Param("is_active") Integer isActive, Pageable pageable);

    @Query("SELECT p FROM Posts p LEFT JOIN p.postOfCommentId pc ON p.id = pc.post WHERE p.moderationStatus= :acceptedType GROUP BY p.id, p.title ORDER BY COUNT(pc) DESC")
    List<Posts> findAllOrderByCommentsDesc(@Param("acceptedType") PostsTypes acceptedType, Pageable pageable);
}
