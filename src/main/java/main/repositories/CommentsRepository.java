package main.repositories;

import main.model.PostComments;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface CommentsRepository extends PagingAndSortingRepository<PostComments, Integer> {

    PostComments findByTime(Date time);

    List<PostComments> findByPostId (Integer id, Sort sort);

    List<PostComments> findByPostId (Integer id);
}
