package main.repositories;

import main.model.CaptchaCodes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaptchaRepository extends JpaRepository<CaptchaCodes, Integer> {
    CaptchaCodes findBySecretCode(String secret);
}
