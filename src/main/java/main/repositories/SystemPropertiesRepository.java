package main.repositories;

import main.model.SystemProperties;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SystemPropertiesRepository extends JpaRepository<SystemProperties, Integer> {
    SystemProperties findByPropertyName (String code);
}

