package main.repositories;

import main.model.Users;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<Users, Integer> {
    Users findByMail(String mail);
    List<Users> findByIsModerator(@Param("is_moderator") int isModerator);
}
