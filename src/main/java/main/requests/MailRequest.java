package main.requests;

import lombok.Data;

@Data
public class MailRequest {
    private String email;
}
