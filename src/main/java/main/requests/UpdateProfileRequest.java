package main.requests;

import lombok.Data;
import org.springframework.web.bind.annotation.RequestParam;

@Data
public class UpdateProfileRequest {

    private String email;

    private Integer removePhoto;

    private String name;

    private String password;
}
