package main.requests;

import lombok.Data;

@Data
public class NewPostRequest {

    private String title;

    private String text;

    private String[] tags;

    private Integer active;

    private Long timestamp;
}
