package main.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ModeratePostRequest {

    @JsonProperty("post_id")
    private Integer id;

    private String decision;
}
