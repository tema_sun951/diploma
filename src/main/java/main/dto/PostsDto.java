package main.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PostsDto {

    private Integer count;

    private List<Post> posts;

    public PostsDto(Integer count, List<Post> posts) {
        this.count = count;
        this.posts = posts;
    }

    @Data
    @NoArgsConstructor
    public static class Post extends AbstractPostDto {

        private String announce;

        private Integer commentCount;

        private PostsDto.Post.User user;

        @Data
        @NoArgsConstructor
        public static class User extends AbstractUserDto {

            public User(Integer id, String name) {
                this.name = name;
                this.id = id;
            }
        }

    }
}
