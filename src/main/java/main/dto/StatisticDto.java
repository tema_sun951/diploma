package main.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StatisticDto {

    private Integer postsCount;

    private Integer likesCount;

    private Integer dislikesCount;

    private Integer viewsCount;

    private Long firstPublication;

    public StatisticDto(Integer postsCount, Integer likesCount, Integer dislikesCount, Integer viewsCount) {
        this.postsCount = postsCount;
        this.likesCount = likesCount;
        this.dislikesCount = dislikesCount;
        this.viewsCount = viewsCount;
    }
}
