package main.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserAdvancedDto extends AbstractUserDto {

    private Boolean moderation;

    private String photo;

    private Integer moderationCount;

    private Boolean settings;
}
