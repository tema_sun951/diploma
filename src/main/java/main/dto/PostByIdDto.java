package main.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PostByIdDto extends AbstractPostDto {

    private Boolean active;

    private User user;

    private String text;

    private List<Comment> comments;

    private List<String> tags;

    @Data
    @NoArgsConstructor
    public static class User {
        private Integer id;

        private String name;
    }

    @Data
    @NoArgsConstructor
    public static class Comment {
        private Integer id;

        private Long timestamp;

        private String text;

        private ComUser user;

        public Comment(Integer id, Long timestamp, String text, ComUser user) {
            this.id = id;
            this.timestamp = timestamp;
            this.text = text;
            this.user = user;
        }

        @Data
        @NoArgsConstructor
        public static class ComUser extends AbstractUserDto {

            private String photo;

            public ComUser(Integer id, String name, String photo) {
                this.id = id;
                this.name = name;
                this.photo = photo;
            }
        }
    }

    @Data
    @NoArgsConstructor
    public static class Tag {
        private String name;
    }
}
