package main.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class PostsByCalendarDto {

    private List<Integer> years;

    private Map<String, Integer> posts;
}
