package main.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class CheckDto extends ResultDto {

    private User user;

    public CheckDto() {
    }

    public CheckDto(boolean result) {
        this.result = result;
    }

    public CheckDto(boolean result, User user) {
        this.result = result;
        this.user = user;
    }

    @Data
    @NoArgsConstructor
    public static class User extends AbstractUserAdvancedDto {

        private String email;
    }
}
