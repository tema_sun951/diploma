package main.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class ResultWithErrorsDto {

    private boolean result;

    private Map<String, String> errors;

    public ResultWithErrorsDto(boolean result, Map<String, String> errors) {
        this.result = result;
        this.errors = errors;
    }

    public ResultWithErrorsDto(boolean result) {
        this.result = result;
    }
}
