package main.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PostIdDto {

    @JsonProperty("post_id")
    private Integer postId;

    public PostIdDto(Integer postId) {
        this.postId = postId;
    }
}
