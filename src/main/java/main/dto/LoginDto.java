package main.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class LoginDto extends ResultDto {

    private User user;

    private List<String> errors;

    public LoginDto(boolean result, List<String> errors) {
        this.result = result;
        this.errors = errors;
    }

    public LoginDto(boolean result, User user) {
        this.result = result;
        this.user = user;
    }

    public LoginDto(boolean result) {
        this.result = result;
    }

    @Data
    @NoArgsConstructor
    public static class User extends AbstractUserAdvancedDto {

        @JsonProperty("e_mail")
        private String mail;
    }
}
