package main.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class TagsDto {

    private List<Tag> tags;

    @Data
    @NoArgsConstructor
    public static class Tag {

        private String name;

        private Float weight;
    }
}
