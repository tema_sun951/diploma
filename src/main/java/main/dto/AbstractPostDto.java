package main.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AbstractPostDto {

    Integer id;

    Long timestamp;

    String title;

    Integer likeCount;

    Integer dislikeCount;

    Integer viewCount;
}
