package main.response;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class InitResponse {
    private String title;

    private String subtitle;

    private String phone;

    private String email;

    private String copyright;

    private String copyrightFrom;
}
