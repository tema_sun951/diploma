package main.controllers;

import main.requests.CommentRequest;
import main.requests.SettingsRequest;
import main.response.InitResponse;
import main.dto.*;
import main.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;

@RestController()
@RequestMapping("/api")
public class ApiGeneralController {

    @Autowired
    private InitService initService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private TagsService tagsService;

    @Autowired
    private AuthService authService;

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private CommentsService commentsService;

    @Autowired
    private CalendarService calendarService;

    @Autowired
    private StatisticService statisticService;

    @GetMapping("/init")
    public InitResponse init() {
        return initService.init();
    }

    @GetMapping("/settings")
    public ResponseEntity<SettingsDto> settings() {
        return new ResponseEntity<>(settingsService.getGlobalSettings(), HttpStatus.OK);
    }

    @PutMapping("/settings")
    public ResponseEntity<Void> updateSettings(@RequestBody SettingsRequest settingsRequest,
                                               @AuthenticationPrincipal Principal principal) {
        if (principal == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        if (!settingsService.updateSettings(
                settingsRequest.getMultiuserMode(), settingsRequest.getPostPreModeration(), settingsRequest.getStatisticIsPublic(), principal).getResult()) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @GetMapping("/tag")
    public TagsDto tag(@RequestParam(required = false) String query) {
        return tagsService.getTagsValue(query);
    }

    @PostMapping("/comment")
    public ResponseEntity<Object> postComment(@RequestBody CommentRequest request,
                                              @AuthenticationPrincipal Principal principal) {
        if (principal == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(commentsService.addComment(request.getParentId(), request.getPostId(), request.getText(), principal), HttpStatus.OK);
    }

    @GetMapping("calendar")
    public ResponseEntity<PostsByCalendarDto> getCountPostsInTime(Integer year) {
        return new ResponseEntity<>(calendarService.calendar(year), HttpStatus.OK);
    }

    @GetMapping("statistics/all")
    public ResponseEntity<StatisticDto> allStatistic(@AuthenticationPrincipal Principal principal) {
        if (principal == null || !settingsService.getGlobalSettings().isStatisticIsPublic()) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            return new ResponseEntity<>(statisticService.allStat(), HttpStatus.OK);
        }
    }

    @GetMapping("auth/captcha")
    public ResponseEntity<CaptchaDto> captcha() throws IOException {
        return new ResponseEntity<>(captchaService.getCaptcha(), HttpStatus.OK);
    }
}
