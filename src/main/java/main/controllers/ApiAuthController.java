package main.controllers;

import main.dto.*;
import main.requests.LoginRequest;
import main.requests.MailRequest;
import main.requests.RegisterRequest;
import main.requests.UpdateProfileRequest;
import main.service.AuthService;
import main.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;

@RestController
@RequestMapping("/api/")
public class ApiAuthController {

    @Autowired
    private AuthService authService;

    @Autowired
    private ImageService imageService;

    private static final Logger logger = LogManager.getLogger(ApiAuthController.class);

    private Boolean isFirstTime = true;

    @PostMapping("auth/register")
    public ResponseEntity<?> registration(@RequestBody RegisterRequest registerRequest) {
        return authService.register(registerRequest);
    }

    @PostMapping(path = "auth/login")
    public ResponseEntity<LoginDto> login(@RequestBody LoginRequest user) {
        LoginDto loginDto = authService.login(user);
        if (!loginDto.getResult()) {
            return new ResponseEntity<>(loginDto, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(loginDto, HttpStatus.OK);
    }

    @GetMapping("auth/logout")
    public ResponseEntity<ResultDto> logout() {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                        .getRequest();
        HttpSession session;
        SecurityContextHolder.clearContext();
        session = request.getSession(false);
        if(session != null) {
            session.invalidate();
        }
        for(Cookie cookie : request.getCookies()) {
            cookie.setMaxAge(0);
        }

        return new ResponseEntity<>(new ResultDto(true), HttpStatus.OK);
    }

    @PostMapping(value = "profile/my", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<ResultWithErrorsDto> updateProfileWithoutPic(@RequestBody UpdateProfileRequest request,
                                                                       @AuthenticationPrincipal Principal principal) {
        if (principal == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(
                authService.updateProfile(null, request.getEmail(), request.getRemovePhoto(), request.getName(), request.getPassword(), principal),
                HttpStatus.OK);
    }

    @PostMapping(value = "profile/my", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<ResultWithErrorsDto> updateProfileWithPic(@RequestParam(required = false) MultipartFile photo,
                                                                    @RequestParam(required = false) String email,
                                                                    @RequestParam(required = false) String name,
                                                                    @RequestParam(required = false) String password,
                                                                    @AuthenticationPrincipal Principal principal) throws IOException {
        if (principal == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(
                authService.updateProfile(photo, email, 0, name, password, principal),
                HttpStatus.OK);
    }

    @GetMapping("statistics/my")
    public ResponseEntity<StatisticDto> myStatistic(@AuthenticationPrincipal Principal principal) {
        if (principal == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(authService.myStatistic(principal), HttpStatus.OK);
    }

    @PostMapping("auth/restore")
    public ResponseEntity<ResultWithErrorsDto> restorePass(@RequestBody MailRequest email) {
        ResultWithErrorsDto itemDto = authService.restore(email.getEmail());
        if (!itemDto.isResult()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(itemDto, HttpStatus.OK);
    }

    @PostMapping("auth/password")
    public ResponseEntity<ResultWithErrorsDto> changePass(@RequestParam(required = false) String mail,
                                                          @RequestParam(required = false) String code,
                                                          @RequestParam(required = false, name = "code") String captcha,
                                                          @RequestParam(required = false, name = "secret_code") String captchaSecret,
                                                          @RequestParam(required = false) String password) {

        ResultWithErrorsDto item = authService.passwordChange(password, mail, code, captcha, captchaSecret);
        if (!item.isResult()) {
            return new ResponseEntity<>(item, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(item, HttpStatus.OK);
    }

    @GetMapping("auth/check")
    public ResponseEntity<CheckDto> check(@AuthenticationPrincipal Principal principal) throws IOException {
        if (isFirstTime) {
            logger.info("Копирование картинок на истиный путь на сервере");
            imageService.copyFiles();
            isFirstTime = false;
        }
        if (principal == null) {
            return ResponseEntity.ok(new CheckDto());
        }
        return new ResponseEntity<>(authService.check(principal), HttpStatus.OK);
    }
}
