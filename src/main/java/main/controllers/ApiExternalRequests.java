package main.controllers;

import main.dto.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/external/")
public class ApiExternalRequests {

    private static final Logger logger = LogManager.getLogger(ApiExternalRequests.class);

    @GetMapping("posts.get")
    public ResponseEntity<PostsDto> getPosts() {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
