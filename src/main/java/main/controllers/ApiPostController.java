package main.controllers;

import java.io.IOException;
import java.security.Principal;
import java.text.ParseException;
import java.util.*;

import main.dto.*;
import main.repositories.VotesRepository;
import main.requests.ModeratePostRequest;
import main.requests.NewPostRequest;
import main.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/")
public class ApiPostController {

    @Autowired
    private PostsService postsService;

    @Autowired
    private AuthService authService;

    @Autowired
    private VotesService votesService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private VotesRepository votesRepository;

    @GetMapping("post")
    public ResponseEntity<PostsDto> getApiPost(@RequestParam Integer offset,
                                               @RequestParam Integer limit,
                                               @RequestParam String mode) {
        return new ResponseEntity<>(postsService.getPosts(offset, limit, mode), HttpStatus.OK);
    }

    @GetMapping("post/search")
    public ResponseEntity<PostsDto> search(@RequestParam(required = false) String query,
                                           @RequestParam(required = false) Integer offset,
                                           @RequestParam(required = false) Integer limit) {
        return new ResponseEntity<>(postsService.search(query, offset, limit), HttpStatus.OK);
    }

    @GetMapping("post/byDate")
    public ResponseEntity<PostsDto> getByDate(@RequestParam(required = false) Integer offset,
                                              @RequestParam(required = false) Integer limit,
                                              @RequestParam(required = false) String date) throws ParseException {
        return new ResponseEntity<>(postsService.getByDate(offset, limit, date), HttpStatus.OK);
    }

    @GetMapping("post/byTag")
    public ResponseEntity<PostsDto> getByTag(@RequestParam(required = false) int offset,
                                             @RequestParam(required = false) int limit,
                                             @RequestParam(required = false) String tag) {
        return new ResponseEntity<>(postsService.getByTag(offset, limit, tag), HttpStatus.OK);
    }

    @GetMapping("post/moderation")
    public ResponseEntity<PostsDto> postListOnModeration(
            @RequestParam(required = false) Integer offset,
            @RequestParam(required = false) Integer limit,
            @RequestParam(name = "status", required = false) String moderationStatus,
            @AuthenticationPrincipal Principal principal) {
        if (principal != null) {
            return new ResponseEntity<>(postsService
                    .moderation(offset, limit, moderationStatus, principal), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("moderation")
    public ResponseEntity<ResultDto> moderationPost(@RequestBody ModeratePostRequest post,
                                                    @AuthenticationPrincipal Principal principal) {
        if (principal == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            return new ResponseEntity<>(
                    postsService.moderatePost(post.getId(), post.getDecision(), principal),
                    HttpStatus.OK
            );
        }
    }

    @GetMapping("post/{id}")
    public ResponseEntity<PostByIdDto> getPostById(@PathVariable int id) {
        PostByIdDto post = postsService.getById(id);
        if (post == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(post, HttpStatus.OK);
        }
    }

    @PutMapping("post/{id}")
    public ResponseEntity<ResultWithErrorsDto> changePostById(@PathVariable Integer id,
                                                              @RequestParam(required = false) Integer active,
                                                              @RequestParam(required = false) String title,
                                                              @RequestParam(required = false) List<String> tags,
                                                              @RequestParam(required = false) String text,
                                                              @RequestParam(required = false) Long timestamp,
                                                              @AuthenticationPrincipal Principal principal) {
        if (principal == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        ResultWithErrorsDto post = postsService.updatePostById(id, active, title, tags, text, timestamp, principal);
        if (!post.isResult()) {
            return new ResponseEntity<>(post, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(post, HttpStatus.OK);
    }

    @PostMapping("post")
    public ResponseEntity<ResultWithErrorsDto> addPost(@RequestBody NewPostRequest newPostRequest,
                                                       @AuthenticationPrincipal Principal principal) {
        if (principal == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            return new ResponseEntity<>(postsService.addPost(newPostRequest, principal), HttpStatus.OK);
        }
    }

    @PostMapping("image")
    public ResponseEntity<?> postImage(@RequestParam(required = false) MultipartFile image,
                                            @AuthenticationPrincipal Principal principal) throws IOException {
        ResponseEntity<?> response = imageService.addImage(image, principal, false);
        if(response.getStatusCode().value() != 200){
            return ResponseEntity.ok(Objects.requireNonNull(response.getBody()));
        }
        return response;
    }

    @PostMapping("post/like")
    public ResponseEntity<ResultDto> postLike(@RequestBody PostIdDto postId,
                                              @AuthenticationPrincipal Principal principal) {
        if (principal == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        ResultDto resultDto = votesService.postVote(postId.getPostId(), principal, 1);
        if (!resultDto.getResult()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(resultDto, HttpStatus.OK);
    }

    @PostMapping("post/dislike")
    public ResponseEntity<ResultDto> postDislike(@RequestBody PostIdDto postId,
                                                 @AuthenticationPrincipal Principal principal) {
        if (principal == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        ResultDto resultDto = votesService.postVote(postId.getPostId(), principal, -1);
        if (!resultDto.getResult()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(resultDto, HttpStatus.OK);
    }

    @GetMapping("post/my")
    public ResponseEntity<PostsDto> myPosts(@RequestParam(required = false) Integer offset,
                                            @RequestParam(required = false) Integer limit,
                                            @RequestParam(required = false) String status,
                                            @AuthenticationPrincipal Principal principal) {
        if (principal == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>
                (postsService.getMyPosts(offset, limit, status, principal), HttpStatus.OK);
    }
}
