package main.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import main.model.types.PostStatus;
import main.model.types.PostsTypes;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class Posts extends AbstractPost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "is_active")
    private Integer isActive;

    @Column(name = "moderation_status",
            columnDefinition = "enum('NEW','ACCEPTED','DECLINED')", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PostsTypes moderationStatus;

    @Column(name = "post_status",
            columnDefinition = "enum('inactive','pending','declined','published')", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PostStatus postStatus;

    private String title;

    @Column(name = "view_count")
    private Integer viewCount;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Users userId;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "moderator_id", referencedColumnName = "id")
    private Users moderatorId;

    @OneToMany
    @Column(name = "posts_votes_id")
    @JoinColumn(name = "id")
    private List<PostVotes> postVotesId = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Tag2Post",
            joinColumns = {@JoinColumn(name = "post_id")},
            inverseJoinColumns =  {@JoinColumn(name = "tag_id")}
    )
    private List<Tags> tags;

    @OneToMany
    @Column(name = "post_of_comment_id")
    @JoinColumn(name = "id")
    private List<PostComments> postOfCommentId = new ArrayList<>();
}
