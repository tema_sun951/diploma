package main.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Tags {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Tag2Post",
            joinColumns = {@JoinColumn(name = "tag_id")},
            inverseJoinColumns =  {@JoinColumn(name = "post_id")})
    private List<Posts> posts;

    private String name;

    public Tags(String name) {
        this.name = name;
    }
}
