package main.model.constants;

public class Constants {

    public static final String TIME_STR = "time";
    public static final String POPULAR_POSTS_STR = "popular";
    public static final String EARLY_POSTS_STR = "early";
    public static final String BEST_POSTS_STR = "best";
    public static final String VIEW_COUNT_POST_STR= "viewCount";
    public static final String MULTIUSER_MODE_STR = "MULTIUSER_MODE";
    public static final String POST_PREMODERATION_STR = "POST_PREMODERATION";
    public static final String STATISTICS_IS_PUBLIC_STR = "STATISTICS_IS_PUBLIC";
    public static final String YES = "YES";
    public static final String NO = "NO";
}
