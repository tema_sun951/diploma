package main.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "is_moderator")
    private boolean isModerator;

    @Column(name = "reg_time")
    private Date regTime;

    private String name;

    private String mail;

    private String password;

    private String code;

    private String photo;

    @OneToMany
    @Column(name = "user_posts_id")
    @JoinColumn(name = "id")
    private List<Posts> userPostsId = new ArrayList<>();

    @OneToMany
    @Column(name = "moderator_posts_id")
    @JoinColumn(name = "id")
    private List<Posts> moderatorPostsId = new ArrayList<>();

    @OneToMany
    @Column(name = "user_posts_votes_id")
    @JoinColumn(name = "id")
    private List<Posts> userPostsVotesId = new ArrayList<>();

    @OneToMany
    @Column(name = "user_post_comments_id")
    @JoinColumn(name = "id")
    private List<PostComments> userPostCommentsId = new ArrayList<>();

    public Role getRole(){
        return isModerator ? Role.MODERATOR : Role.USER;
    }
}
