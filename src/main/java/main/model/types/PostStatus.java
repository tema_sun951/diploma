package main.model.types;

public enum PostStatus {
    inactive,
    pending,
    declined,
    published
}
