package main.service;

import main.response.InitResponse;
import org.springframework.stereotype.Service;

@Service
public class InitService {

    public InitResponse init(){
        InitResponse initResponse = new InitResponse();
        initResponse.setPhone("+7 996 427 35 39");
        initResponse.setTitle("DevPub");
        initResponse.setSubtitle("Рассказы разработчиков");
        initResponse.setCopyright("copyright");
        initResponse.setEmail("ya.temasun@yandex.ru");
        initResponse.setCopyrightFrom("2021");
        return initResponse;
    }
}
