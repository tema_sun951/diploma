package main.service;

import main.dto.ResultDto;
import main.dto.SettingsDto;
import main.exceptions.ModeratorException;
import main.model.GlobalSettings;
import main.model.constants.Constants;
import main.repositories.SettingsRepository;
import main.repositories.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class SettingsService {

    @Autowired
    private SettingsRepository settingsRepository;

    @Autowired
    private UserRepository userRepository;

    private static final Logger logger = LogManager.getLogger(SettingsService.class);

    private GlobalSettings multiuserSetting;
    private GlobalSettings premoderSetting;
    private GlobalSettings statisticSetting;

    public void init() {
        if (multiuserSetting != null && premoderSetting != null && statisticSetting != null) {
            return;
        }
        multiuserSetting = settingsRepository.findByCode(Constants.MULTIUSER_MODE_STR);
        premoderSetting = settingsRepository.findByCode(Constants.POST_PREMODERATION_STR);
        statisticSetting = settingsRepository.findByCode(Constants.STATISTICS_IS_PUBLIC_STR);
    }

    public SettingsDto getGlobalSettings() {
        init();
        return new SettingsDto(
                multiuserSetting.getValue().equals(Constants.YES),
                premoderSetting.getValue().equals(Constants.YES),
                statisticSetting.getValue().equals(Constants.YES)
        );
    }

    public ResultDto updateSettings(Boolean multiuserMode,
                                    Boolean postPreModeration,
                                    Boolean statisticIsPublic,
                                    Principal principal) {
        if (!userRepository.findByMail(principal.getName()).isModerator()) {
            throw new ModeratorException();
        }
        init();

        multiuserSetting.setValue(multiuserMode ? Constants.YES : Constants.NO);
        settingsRepository.save(multiuserSetting);

        premoderSetting.setValue(postPreModeration ? Constants.YES : Constants.NO);
        settingsRepository.save(premoderSetting);

        statisticSetting.setValue(statisticIsPublic ? Constants.YES : Constants.NO);
        settingsRepository.save(statisticSetting);

        return new ResultDto(true);
    }
}
