package main.service;

import main.dto.TagsDto;
import main.model.Posts;
import main.model.Tags;
import main.repositories.PostsRepository;
import main.repositories.Tags2PostsRepository;
import main.repositories.TagsRepository;
import main.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TagsService {

    @Autowired
    private TagsRepository tagsRepository;

    @Autowired
    private Tags2PostsRepository tags2PostsRepository;

    public TagsDto getTagsValue(String query) {
        TagsDto tagsDTO = new TagsDto();
        List<TagsDto.Tag> tagsArr = new LinkedList<>();
        Iterable<Tags> tagsIterable;

        if (query == null || query.length() == 0) {
            tagsIterable = tagsRepository.findAll();
        } else {
            tagsIterable = tagsRepository.findByNameContaining(query, Sort.by("name"));
        }
        for (Tags t : tagsIterable) {
            TagsDto.Tag tag = new TagsDto.Tag();
            tag.setName(t.getName());
            float weight = (float)(tags2PostsRepository.findByTagId(t.getId()).size()) / (float)(tags2PostsRepository.count()) * 2;
            tag.setWeight(weight < 0.3 ? (float)0.3 : weight);
            tagsArr.add(tag);
        }
        tagsDTO.setTags(tagsArr);
        return tagsDTO;
    }
}
