package main.service;

import main.dto.*;
import main.exceptions.ModeratorException;
import main.model.*;
import  main.model.constants.Constants;
import main.model.types.PostStatus;
import main.model.types.PostsTypes;
import main.repositories.*;
import main.requests.NewPostRequest;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.text.ParseException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;

@Service
public class PostsService {

    @Autowired
    private PostsRepository postsRepository;

    @Autowired
    private TagsRepository tagsRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthService authService;

    @Autowired
    private Tags2PostsRepository tags2PostsRepository;

    @Autowired
    private CommentsRepository commentsRepository;

    @Autowired
    private VotesRepository votesRepository;

    private PostsDto generatePostsDto(Iterable<Posts> posts, PostsTypes type) {
        PostsDto postsDTO = new PostsDto();
        List<PostsDto.Post> postsArr = new LinkedList<>();
        for (Posts p : posts) {
            if (!p.getModerationStatus().equals(type)) {
                continue;
            }
            PostsDto.Post post = new PostsDto.Post();
            PostsDto.Post.User user = new PostsDto.Post.User();

            int likeCount = 0;
            int dislikeCount = 0;
            for (PostVotes vo : votesRepository.findByPostId(p.getId())) {
                if (vo.getValue() == -1) {
                    dislikeCount++;
                } else if (vo.getValue() == 1){
                    likeCount++;
                }
            }

            user.setId(p.getUserId().getId());
            user.setName(p.getUserId().getName());

            post.setId(p.getId());
            if (p.getTime() != null) {
                post.setTimestamp(p.getTime().getTime() / 1000);
            }
            post.setUser(user);
            post.setTitle(p.getTitle());
            if (p.getText() != null) {
                String text = Jsoup.parse(p.getText()).text();
                post.setAnnounce( text.length() > 1000 ? text.substring(0, 1000) : text);
            }
            post.setLikeCount(likeCount);
            post.setDislikeCount(dislikeCount);
            post.setCommentCount(commentsRepository.findByPostId(p.getId()).size() == 0 ? 0 : commentsRepository.findByPostId(p.getId()).size());
            post.setViewCount(p.getViewCount());
            postsArr.add(post);
        }

        postsDTO.setCount(postsArr.size());
        postsDTO.setPosts(postsArr);
        return postsDTO;
    }

    public PostsDto getPosts(int offset, int limit, String mode) {
        Pageable pageable = PageRequest.of(offset, limit, Sort.by(Constants.TIME_STR).descending());
        if (mode.equals(Constants.POPULAR_POSTS_STR)) {
            return generatePostsDto(postsRepository.findAllOrderByCommentsDesc(PostsTypes.ACCEPTED, pageable), PostsTypes.ACCEPTED);
        } else if (mode.equals(Constants.EARLY_POSTS_STR)) {
            pageable = PageRequest.of(offset, limit, Sort.by(Constants.TIME_STR));
        } else if (mode.equals(Constants.BEST_POSTS_STR)) {
            pageable = PageRequest.of(offset, limit, Sort.by(Constants.VIEW_COUNT_POST_STR).descending());
        }
        return generatePostsDto(postsRepository.findAllByModerationStatus(PostsTypes.ACCEPTED, pageable), PostsTypes.ACCEPTED);
    }

    public ResultWithErrorsDto addPost(NewPostRequest newPostRequest, Principal principal) {
        ResultWithErrorsDto resultWithErrorsDTO = new ResultWithErrorsDto();
        List<Tags> tagsList = new ArrayList<>();
        Map<String, String> errorsMap = new HashMap<>();
        if (newPostRequest.getTimestamp() == null) {
            errorsMap.put("timestamp", "Не указано время размещения поста");
        } else if (newPostRequest.getTitle() == null) {
            errorsMap.put("title", "Не указан заголовок");
        } else if (newPostRequest.getText() == null) {
            errorsMap.put("text", "Отсутствует текст поста");
        }
        if (!errorsMap.isEmpty()) {
            return new ResultWithErrorsDto(false, errorsMap);
        }
        Users user = userRepository.findByMail(principal.getName());
        Posts post = new Posts();
        post.setTime(new Date(newPostRequest.getTimestamp() * 1000));
        post.setIsActive(newPostRequest.getActive());
        post.setPostStatus(newPostRequest.getActive() == 1 ? PostStatus.pending : PostStatus.inactive);
        post.setTitle(newPostRequest.getTitle());
        post.setText(newPostRequest.getText());
        for (String tagName : newPostRequest.getTags()) {
            System.out.println(tagName);
            Tags tag = tagsRepository.findByName(tagName, Sort.by("name"));
            if (tag == null) {
                tag = new Tags();
                tag.setName(tagName);
                tagsRepository.save(tag);
            }
            tagsList.add(tag);
        }
        post.setTags(tagsList);
        post.setModerationStatus(PostsTypes.NEW);
        post.setUserId(user);
        post.setViewCount(0);
        postsRepository.save(post);
        resultWithErrorsDTO.setResult(true);
        return resultWithErrorsDTO;
    }

    public PostsDto search(String query, Integer offset, Integer limit) {
        Pageable pageable = PageRequest.of(offset, limit, Sort.by(Constants.TIME_STR));
        Iterable<Posts> posts = postsRepository.findByTitleContainingOrTextContaining(query, query, pageable);
        return generatePostsDto(posts, PostsTypes.ACCEPTED);
    }

    public PostsDto getByDate(Integer offset, Integer limit, String dateStr) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        formatter = formatter.withLocale(Locale.getDefault());
        Pageable pageable = PageRequest.of(offset, limit, Sort.by(Constants.TIME_STR));
        Iterable<Posts> posts = postsRepository.findByTimeBetween(
                Date.from(LocalDate.parse(dateStr, formatter).atTime(0, 0, 0).atZone(ZoneId.systemDefault()).toInstant()),
                Date.from(LocalDate.parse(dateStr, formatter).atTime(23, 59, 59).atZone(ZoneId.systemDefault()).toInstant()),
                pageable);
        return generatePostsDto(posts, PostsTypes.ACCEPTED);
    }

    public PostsDto getByTag(int offset, int limit, String tag) {
        Pageable pageable = PageRequest.of(offset, limit, Sort.by(Constants.TIME_STR));
        Iterable<Posts> posts = postsRepository.findByTags(tagsRepository.findByName(tag), pageable);
        return generatePostsDto(posts, PostsTypes.ACCEPTED);
    }

    public PostByIdDto getById(int id) {
        PostByIdDto postDto = new PostByIdDto();
        PostByIdDto.Comment.ComUser commentUser = new PostByIdDto.Comment.ComUser();
        PostByIdDto.User userDto = new PostByIdDto.User();

        Optional<Posts> optionalPosts = postsRepository.findById(id);
        if (optionalPosts.isPresent()) {
            Posts post = optionalPosts.get();
            post.setViewCount(post.getViewCount() + 1);
            postsRepository.save(post);

            userDto.setId(post.getUserId().getId());
            userDto.setName(post.getUserId().getName());

            List<PostByIdDto.Comment> commentsDtoList = new LinkedList<>();
            for (PostComments com : commentsRepository.findByPostId(post.getId(), Sort.by(Constants.TIME_STR))) {
                commentUser.setId(com.getUser().getId());
                commentUser.setName(com.getUser().getName());
                commentUser.setPhoto(com.getUser().getPhoto());

                PostByIdDto.Comment comment = new PostByIdDto.Comment();
                comment.setId(com.getId());
                comment.setTimestamp(com.getTime().getTime() / 1000);
                comment.setText(com.getText());
                comment.setUser(commentUser);
                commentsDtoList.add(comment);
            }

            List<String> tagsDtoList = new LinkedList<>();
            for (Tags t : post.getTags()) {
                PostByIdDto.Tag tag = new PostByIdDto.Tag();
                tag.setName(t.getName());
                tagsDtoList.add(tag.getName());
            }

            postDto.setId(post.getId());
            postDto.setTimestamp(post.getTime().getTime() / 1000);
            postDto.setActive(post.getIsActive() == 1);
            postDto.setUser(userDto);
            postDto.setTitle(post.getTitle());
            postDto.setText(post.getText());
            postDto.setLikeCount((int) (votesRepository.findByPostId(post.getId()).stream().filter(vote -> vote.getValue() == 1).count()));
            postDto.setDislikeCount((int) (votesRepository.findByPostId(post.getId()).stream().filter(vote -> vote.getValue() == -1).count()));
            postDto.setViewCount(post.getViewCount());
            postDto.setComments(commentsDtoList);
            postDto.setTags(tagsDtoList);
        }
        return postDto;
    }

    public ResultWithErrorsDto updatePostById(Integer id,
                                              Integer active,
                                              String title,
                                              List<String> tags,
                                              String text,
                                              Long timestamp,
                                              Principal principal) {
        Map<String, String> errorsMap = new HashMap<>();
        Optional<Posts> optPost = postsRepository.findById(id);

        Posts post = new Posts();
        if (optPost.isPresent()) {
            post = optPost.get();
        } else {
            errorsMap.put("id", "Нет поста с таким id");
            return new ResultWithErrorsDto(false, errorsMap);
        }
        if (text.length() < 6) {
            errorsMap.put("text", "Текст поста должен содержать больше 6 символов");
        }
        if (active != null)
            post.setIsActive(active);
        if (title != null)
            post.setTitle(title);
        if (tags != null) {
            List<Tags> tagsList = new ArrayList<>();
            tags.forEach(tag -> tagsList.add(new Tags(tag)));
            post.setTags(tagsList);
        }
        if (text != null)
            post.setText(text);
        if (timestamp != null)
            post.setTime(new Date(timestamp));

        postsRepository.save(post);
        return new ResultWithErrorsDto(true);
    }

    public PostsDto moderation(Integer offset,
                               Integer limit,
                               String moderationStatus,
                               Principal principal) {
        Users user = userRepository.findByMail(principal.getName());
        if (!user.isModerator()) {
            throw new ModeratorException();
        }
        Pageable pageable = PageRequest.of(offset, limit, Sort.by(Constants.TIME_STR));
        Iterable<Posts> list = postsRepository.findByModerationStatusAndIsActive(
                PostsTypes.valueOf(moderationStatus.toUpperCase(Locale.ROOT)), 1, pageable);
        if (moderationStatus.toUpperCase(Locale.ROOT).equals(PostsTypes.NEW.toString()))
            return generatePostsDto(list, PostsTypes.NEW);
        if (moderationStatus.toUpperCase(Locale.ROOT).equals(PostsTypes.ACCEPTED.toString()))
            return generatePostsDto(list, PostsTypes.ACCEPTED);
        if (moderationStatus.toUpperCase(Locale.ROOT).equals(PostsTypes.DECLINED.toString()))
            return generatePostsDto(list, PostsTypes.DECLINED);
        return null;
    }

    public ResultDto moderatePost(Integer id, String decision, Principal principal) {
        if (!userRepository.findByMail(principal.getName()).isModerator()) {
            throw new ModeratorException();
        }
        Optional<Posts> postsOptional = postsRepository.findById(id);
        if (postsOptional.isEmpty()) {
            return new ResultDto(false);
        }
        Posts post = postsOptional.get();
        if (decision.equals("accept")) {
            post.setModerationStatus(PostsTypes.ACCEPTED);
            post.setPostStatus(PostStatus.published);
        } else {
            post.setModerationStatus(PostsTypes.DECLINED);
            post.setPostStatus(PostStatus.declined);
        }
        postsRepository.save(post);
        return new ResultDto(true);
    }


    public PostsDto getMyPosts(Integer offset,
                               Integer limit,
                               String status,
                               Principal principal) {
        Users user = userRepository.findByMail(principal.getName());
        Pageable pageable = PageRequest.of(offset, limit, Sort.by(Constants.TIME_STR));
        List<Posts> postsList = postsRepository.findByUserIdAndPostStatus(user.getId(), PostStatus.valueOf(status), pageable);
        List<PostsDto.Post> filteredList = new LinkedList<>();
        for (Posts post : postsList) {
            filteredList.add(authService.generatePost(post, user));
        }
        return new PostsDto(filteredList.size(), filteredList);
    }

}
