package main.service;

import com.github.cage.Cage;
import com.github.cage.GCage;
import main.dto.CaptchaDto;
import main.model.CaptchaCodes;
import main.repositories.CaptchaRepository;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;
import java.util.Date;


@Service
public class CaptchaService {

    private String cageStr = "";
    static final int IMG_WIDTH = 100;
    static final int IMG_HEIGHT = 35;

    @Autowired
    private CaptchaRepository captchaRepository;

    private File generateCaptchaImg() throws IOException {
        Cage cage = new GCage();
        String pathname = "src" + File.separator +
                "main" + File.separator +
                "resources" + File.separator +
                "captchaImg" + File.separator +
                (int)(Math.random() * 1_000_000) + "captca.jpg";
        try (OutputStream os = new FileOutputStream(pathname, false)) {
            cageStr = cage.getTokenGenerator().next();
            cage.draw(cageStr, os);
        }
        File file = new File(pathname);
        BufferedImage originalImage = ImageIO.read(file);
        int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();

        BufferedImage resizeImageJpg = resizeImage(originalImage, type);
        ImageIO.write(resizeImageJpg, "jpg", new File(pathname));
        return file;
    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int type) {
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();

        return resizedImage;
    }

    private String fileToBaseStr(File file) throws IOException {
        byte[] fileContent = FileUtils.readFileToByteArray(file);
        return Base64.getEncoder().encodeToString(fileContent);
    }

    public CaptchaDto getCaptcha() throws IOException {
        //delete old captcha
        String pathname = "src" + File.separator +
                "main" + File.separator +
                "resources" + File.separator +
                "captchaImg";
        for (File file: new File(pathname).listFiles()) {
            if (file.isFile()) {
                file.delete();
            }
        }

        //Create captcha
        CaptchaDto captchaDto = new CaptchaDto();
        File file = generateCaptchaImg();
        String baseStr = "data:image/png;base64, " + fileToBaseStr(file);
        String secret = String.valueOf(Math.random());
        captchaDto.setImage(baseStr);
        captchaDto.setSecret(secret);
        CaptchaCodes captchaCodes = new CaptchaCodes();
        captchaCodes.setTime(new Date());
        captchaCodes.setCode(cageStr);
        captchaCodes.setSecretCode(secret);
        captchaRepository.save(captchaCodes);
        return captchaDto;
    }
}

