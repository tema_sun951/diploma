package main.service;

import main.dto.ResultWithErrorsDto;
import main.model.PostComments;
import main.model.Users;
import main.repositories.CommentsRepository;
import main.repositories.PostsRepository;
import main.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class CommentsService {

    @Autowired
    private CommentsRepository commentsRepository;

    @Autowired
    private PostsRepository postsRepository;

    @Autowired
    private UserRepository userRepository;

    public Object addComment(Integer parentId, Integer postId, String text, Principal principal) {
        Map<String, String> errorsMap = new HashMap<>();
        if (postsRepository.findById(postId).isEmpty()){
            errorsMap.put("post", "Поста не существует");
        } else if (parentId != null && commentsRepository.findById(parentId).isEmpty()){
            errorsMap.put("comment", "Комментария не существует");
        } else if (text.trim().length() == 0){
            errorsMap.put("text", "Текста комментария нет");
        }
        if (text.length() < 2){
            errorsMap.put("text", "Текст должен содержать больше двух символов");
        }
        if (!errorsMap.isEmpty()){
            return new ResultWithErrorsDto(false, errorsMap);
        }
        Users user = userRepository.findByMail(principal.getName());
        Date time = new Date();
        PostComments comment = new PostComments();
        comment.setParentId(parentId == null? 0 : parentId);
        comment.setUser(user);
        comment.setPost(postsRepository.findById(postId).get());
        comment.setTime(time);
        comment.setText(text);
        commentsRepository.save(comment);

        return new HashMap<>().put("post_id", postId);
    }
}
