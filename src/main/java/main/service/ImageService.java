package main.service;

import main.dto.ResultWithErrorsDto;
import main.model.Users;
import main.repositories.PostsRepository;
import main.repositories.Tags2PostsRepository;
import main.repositories.TagsRepository;
import main.repositories.UserRepository;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.Principal;
import java.util.*;
import java.util.List;

@Service
public class ImageService {

    @Autowired
    private PostsRepository postsRepository;

    @Autowired
    private TagsRepository tagsRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthService authService;

    @Autowired
    private Tags2PostsRepository tags2PostsRepository;

    private final String IMAGE_STR = "image";

    public ResponseEntity<?> addImage(MultipartFile img,
                                      Principal principal,
                                      Boolean changeProfile) throws IOException {
        String filename = StringUtils.cleanPath(Objects.requireNonNull(img.getOriginalFilename()));
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        Map<String, String> errors = validateFile(img, filename);
        if (errors.size() > 0) {
            return ResponseEntity.badRequest().body(new ResultWithErrorsDto(false, errors));
        }

        String path ="/upload/" + img.getOriginalFilename();
        String realPath = request.getServletContext().getRealPath(path);
        String srcPath = "src/main/resources/" + path;
        byte[] photo = img.getBytes();
        File file = new File(realPath);
        FileUtils.writeByteArrayToFile(file, photo);

        File srcFile = new File(srcPath);
        FileUtils.writeByteArrayToFile(srcFile, photo);

        File postFile = new File(request.getServletContext().getRealPath("post/" + path));
        FileUtils.writeByteArrayToFile(postFile, photo);

        String result = path.replace('\\', '/');
        if (changeProfile) {
            Users user = userRepository.findByMail(principal.getName());
            user.setPhoto(result);
            userRepository.save(user);
            BufferedImage image = ImageIO.read(file);
            ImageIO.write(createResizedCopy(image, 300, 500), "jpg", file);
            BufferedImage srcImage = ImageIO.read(file);
            ImageIO.write(createResizedCopy(srcImage, 300, 500), "jpg", srcFile);
        }
        return ResponseEntity.ok(result);
    }

    private BufferedImage createResizedCopy(Image originalImage,
                                            int scaledWidth,
                                            int scaledHeight) {
        BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = scaledBI.createGraphics();
        g.setComposite(AlphaComposite.Src);
        g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
        g.dispose();
        return scaledBI;
    }

    public void copyFiles() throws IOException {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                        .getRequest();
        String realPath = request.getServletContext().getRealPath("upload/");
        String srcPath = "src/main/resources/upload/";
        File dir = new File(srcPath);
        List<File> list = new ArrayList<>();
        for ( File file : dir.listFiles() ){
            if ( file.isFile() )
                list.add(file);
        }
        for (File f : list){
            File file = new File(realPath);
            file.mkdir();
            try (InputStream is = new FileInputStream(srcPath + f.getName());
                OutputStream os = new FileOutputStream(realPath + f.getName())) {
                byte[] buffer = new byte[1024];
                int length;
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
            }
        }
    }

    public Map<String, String> validateFile(MultipartFile file, String filename ) {
        Map<String, String> errorsMap = new HashMap<>();
        if (filename.toLowerCase().matches("(?!.*(?:\\.jpe?g|\\.png)$)")) {
            errorsMap.put(IMAGE_STR, "Файлы только jpg, png");
            return errorsMap;
        } else if (file.getSize() > 1242880) {
            errorsMap.put(IMAGE_STR, "Слишком большой фаил");
            return errorsMap;
        } else if (file.isEmpty()) {
            errorsMap.put(IMAGE_STR, "Пустой фаил");
            return errorsMap;
        } else if (filename.contains("..")) {
            errorsMap.put(IMAGE_STR, "Некорректное имя");
            return errorsMap;
        }
        return errorsMap;
    }
}
