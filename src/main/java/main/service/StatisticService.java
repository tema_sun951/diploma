package main.service;

import main.dto.StatisticDto;
import main.model.Posts;
import main.model.Users;
import main.model.types.PostsTypes;
import main.repositories.PostsRepository;
import main.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;

@Service
public class StatisticService {

    @Autowired
    private PostsRepository postsRepository;

    public StatisticDto allStat(){
        StatisticDto statisticDto = new StatisticDto();
        List<Posts> posts = postsRepository.findAll(Sort.by("time"));
        int likeCount = 0;
        int dislikeCount = 0;
        int viewsCount = 0;
        for (Posts post : posts) {
            if (!post.getModerationStatus().equals(PostsTypes.ACCEPTED)){
                continue;
            }
            likeCount += post.getPostVotesId().stream().filter(vote -> vote.getValue() == 1).count();
            dislikeCount += post.getPostVotesId().stream().filter(vote -> vote.getValue() == -1).count();
            viewsCount = post.getViewCount();
        }
        statisticDto.setPostsCount(posts.size());
        statisticDto.setLikesCount(likeCount);
        statisticDto.setDislikesCount(dislikeCount);
        statisticDto.setViewsCount(viewsCount);
        statisticDto.setFirstPublication(posts.get(0).getTime().getTime() / 1000);
        return statisticDto;
    }
}
