package main.service;

import main.dto.ResultDto;
import main.model.PostVotes;
import main.model.Posts;
import main.model.Users;
import main.repositories.PostsRepository;
import main.repositories.UserRepository;
import main.repositories.VotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class VotesService {

    @Autowired
    private PostsRepository postsRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VotesRepository votesRepository;

    public ResultDto postVote(Integer postId, Principal principal, int voteNum){
        Optional<Posts> postOpt = postsRepository.findById(postId);
        Posts post = new Posts();
        if (postOpt.isPresent()) post = postOpt.get();
        Users user = userRepository.findByMail(principal.getName());
        PostVotes postVote = votesRepository.findByUserIdAndPostId(user.getId(), postId);
        if (postVote != null) {
            if (postVote.getValue() == voteNum) {
                postVote.setValue(0);
            } else if (postVote.getValue() == 1){
                postVote.setValue(-1);
            } else {
                postVote.setValue(1);
            }
            return new ResultDto(true);
        }

        PostVotes vote = new PostVotes();
        vote.setUser(user);
        vote.setPost(post);
        vote.setTime(new Date());
        vote.setValue(voteNum);
        votesRepository.save(vote);
        return new ResultDto(true);
    }


}
