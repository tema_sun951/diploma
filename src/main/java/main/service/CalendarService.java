package main.service;

import main.dto.PostsByCalendarDto;
import main.model.Posts;
import main.model.types.PostsTypes;
import main.repositories.PostsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CalendarService {

    @Autowired
    private PostsRepository postsRepository;

    public PostsByCalendarDto calendar(Integer year){
        PostsByCalendarDto postsDto = new PostsByCalendarDto();
        Map<String, Integer> postsForDates = new TreeMap<>();
        List<Posts> postsList = new ArrayList<>();
        if (year == null) {
            List<Integer> calendarList = new ArrayList<>();
            postsRepository.findAll().forEach(postsList::add);
            for (Posts p : postsList){
                if (p.getModerationStatus().equals(PostsTypes.ACCEPTED)) {
                    Date date = p.getTime();
                    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    calendar.setTime(date);
                    int yearCal = calendar.get(Calendar.YEAR);
                    if (!calendarList.contains(yearCal)) {
                        calendarList.add(yearCal);
                    }
                    if (!postsForDates.containsKey(String.valueOf(date))) {
                        postsForDates.put(String.valueOf(date).substring(0, 10), postsRepository.findByTime(p.getTime()).size());
                    }
                }
            }
            postsDto.setYears(calendarList);
            postsDto.setPosts(postsForDates);
        } else {
            postsRepository.findAll().forEach(postsList::add);
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, Calendar.FEBRUARY, 2);
            for (Posts p : postsList){
                if (p.getModerationStatus().equals(PostsTypes.ACCEPTED)) {
                    Date date = p.getTime();
                    Calendar calendarOfPost = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    calendarOfPost.setTime(date);
                    if (calendar.get(Calendar.YEAR) == calendarOfPost.get(Calendar.YEAR)) {
                        if (!postsForDates.containsKey(String.valueOf(date))) {
                            postsForDates.put(String.valueOf(date).substring(0, 10), postsRepository.findByTime(p.getTime()).size());
                        }
                    }
                }
            }
            postsDto.setYears(Collections.singletonList(year));
            postsDto.setPosts(postsForDates);
        }

        return postsDto;
    }
}
