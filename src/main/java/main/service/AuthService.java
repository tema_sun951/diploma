package main.service;

import main.dto.*;
import main.model.CaptchaCodes;
import main.model.PostVotes;
import main.model.Posts;
import main.model.Users;
import main.model.types.PostsTypes;
import main.repositories.CaptchaRepository;
import main.repositories.PostsRepository;
import main.repositories.UserRepository;
import main.requests.LoginRequest;
import main.requests.RegisterRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.*;
import java.util.List;

@Service
public class AuthService {

    private final AuthenticationManager authenticationManager;
    private UserRepository userRepository;

    @Autowired
    private CaptchaRepository captchaRepository;

    @Autowired
    private PostsRepository postsRepository;

    @Autowired
    private PostsService postsService;

    @Autowired
    private MailService mailService;

    @Autowired
    private ImageService imageService;

    @Autowired
    public AuthService(AuthenticationManager authenticationManager, UserRepository userRepository) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
    }


    public LoginDto login(LoginRequest userLoginDto) {
        Authentication auth = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(userLoginDto.getMail(), userLoginDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(auth);
        User userLogin = (User) auth.getPrincipal();
        LoginDto loginDTO = new LoginDto();
        List<String> errorsList = new ArrayList<>();
        if (userLoginDto.getMail() == null || userLoginDto.getPassword() == null) {
            errorsList.add("Email или пароль не введены");
            return new LoginDto(false, errorsList);
        }
        Users user = userRepository.findByMail(userLogin.getUsername());
        if (user == null) {
            loginDTO.setResult(false);
            errorsList.add("Пользователя с таким email не существует");

        } else {
            LoginDto.User userDto = new LoginDto.User();
            userDto.setId(user.getId());
            userDto.setMail(user.getMail());
            userDto.setModeration(user.isModerator());
            userDto.setName(user.getName());
            userDto.setPhoto(user.getPhoto());
            userDto.setModerationCount(user.getModeratorPostsId().size());
            userDto.setSettings(true);
            loginDTO.setResult(true);
            loginDTO.setUser(userDto);
        }
        loginDTO.setErrors(errorsList);

        return loginDTO;
    }

    public ResponseEntity<?> register(RegisterRequest registerRequest) {
        Map<String, String> errorsMap = new HashMap<>();
        ResultWithErrorsDto userDto = new ResultWithErrorsDto();
        if (userRepository.findByMail(registerRequest.getEmail()) != null) {
            errorsMap.put("e-mail", "Этот e-mail уже используется");
        }
        if (registerRequest.getName().length() < 3) {
            errorsMap.put("name", "Имя введено неправильно");
        }
        if (registerRequest.getPassword().length() < 6) {
            errorsMap.put("password", "Пароль короче шести символов");
        }
        if (!registerRequest.getCaptcha().equals
                (captchaRepository.findBySecretCode(registerRequest.getCaptchaSecret()).getCode())) {
            errorsMap.put("captcha", "Код каптчи введен неверно");
        }
        if (!errorsMap.isEmpty()) {
            userDto.setResult(false);
            userDto.setErrors(errorsMap);
            return ResponseEntity.ok(userDto);
        }
        Users user = new Users();
        user.setMail(registerRequest.getEmail());
        user.setPassword(BCrypt.hashpw(registerRequest.getPassword(), BCrypt.gensalt(12)));
        user.setName(registerRequest.getName());
        user.setRegTime(new Date());
        user.setModerator(false);
        userRepository.save(user);
        userDto.setResult(true);
        return ResponseEntity.ok(userDto);
    }

    public ResultWithErrorsDto updateProfile(MultipartFile photo,
                                             String email,
                                             Integer removePhoto,
                                             String name,
                                             String password,
                                             Principal principal) {
        ResultWithErrorsDto itemDto = new ResultWithErrorsDto();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Users user = userRepository.findByMail(principal.getName());
        if (name != null) user.setName(name);
        if (email != null) user.setMail(email);
        if (password != null) user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt(12)));
        if (removePhoto != null && removePhoto.equals(1)) user.setPhoto(null);
        if (photo != null) {
            try {
                imageService.addImage(photo, principal, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        userRepository.save(user);
        itemDto.setResult(true);
        return itemDto;
    }

    public PostsDto.Post generatePost(Posts post, Users user) {
        int likeCount = 0;
        int dislikeCount = 0;
        for (PostVotes vote : post.getPostVotesId()) {
            if (vote.getValue() == 1) {
                likeCount++;
            }
            if (vote.getValue() == -1) {
                dislikeCount++;
            }
        }
        PostsDto.Post postDto = new PostsDto.Post();
        postDto.setId(post.getId());
        postDto.setTimestamp(post.getTime().getTime() / 1000);
        postDto.setUser(new PostsDto.Post.User(user.getId(), user.getName()));
        postDto.setTitle(post.getTitle());
        postDto.setAnnounce(post.getText().length() < 20 ? post.getText() : post.getText().substring(0, 20));
        postDto.setLikeCount(likeCount);
        postDto.setDislikeCount(dislikeCount);
        postDto.setViewCount(post.getViewCount());
        postDto.setViewCount(post.getViewCount());
        return postDto;
    }

    public StatisticDto myStatistic(Principal principal) {
        Users user = userRepository.findByMail(principal.getName());
        StatisticDto statisticDto = new StatisticDto();
        List<Posts> postsList = user.getUserPostsId();
        if (postsList.size() == 0) {
            return new StatisticDto(0, 0, 0, 0);
        }
        int likeCount = 0;
        int dislikeCount = 0;
        int viewCount = 0;
        for (Posts post : postsList) {
            viewCount = +post.getViewCount();
        }
        for (Posts post : postsList) {
            for (PostVotes vote : post.getPostVotesId()) {
                if (vote.getValue() == 1) {
                    likeCount++;
                }
                if (vote.getValue() == -1) {
                    dislikeCount++;
                }
            }
        }
        statisticDto.setPostsCount(postsList.size());
        statisticDto.setViewsCount(viewCount);
        statisticDto.setLikesCount(likeCount);
        statisticDto.setDislikesCount(dislikeCount);
        statisticDto.setFirstPublication(postsList.get(0).getTime().getTime() / 1000);
        return statisticDto;
    }

    public ResultWithErrorsDto restore(String email) {
        Users user = userRepository.findByMail(email);
        if (user == null) {
            return new ResultWithErrorsDto(false);
        }
        String codeForRestore = String.valueOf(hashCode());
        String path = "http://localhost:8080/api/auth/login/change-password/" + codeForRestore;
        user.setCode(codeForRestore);
        mailService.sendSimpleEmail(email, "Для восстановления пароля пройдите по ссылке:" + path);
        return new ResultWithErrorsDto(true);

    }

    public ResultWithErrorsDto passwordChange(String password,
                                              String mail,
                                              String code,
                                              String captcha,
                                              String captchaSecret) {
        Map<String, String> errorsMap = new HashMap<>();
        CaptchaCodes captchaCode = captchaRepository.findBySecretCode(captchaSecret);
        if (mail == null || mail.trim().length() == 0) {
            errorsMap.put("email", "email не введен");
        }
        if (password == null || password.length() == 0) {
            errorsMap.put("password", "пароль не введен");
        }
        if (captchaCode == null) {
            errorsMap.put("secretCode", "Неверный секретный код");
        }
        if (!captchaCode.getCode().equals(captcha)) {
            errorsMap.put("captcha", "Неверный код с картинки");
        }
        Users user = userRepository.findByMail(mail);
        if (!user.getCode().equals(code)) {
            errorsMap.put("code", "невеный код восстановления");
        }
        if (!errorsMap.isEmpty()) {
            return new ResultWithErrorsDto(false, errorsMap);
        }
        user.setPassword(password);
        userRepository.save(user);
        return new ResultWithErrorsDto(true);
    }

    public CheckDto check(Principal principal) {
        Users user = userRepository.findByMail(principal.getName());
        CheckDto.User userDto = new CheckDto.User();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setPhoto(user.getPhoto());
        userDto.setEmail(user.getMail());
        userDto.setModeration(user.isModerator());
        userDto.setModerationCount(userDto.getModeration() ? postsRepository.findByModerationStatusAndIsActive(PostsTypes.NEW, 1).size() : 0);
        userDto.setSettings(true);
        return new CheckDto(true, userDto);
    }
}
